import "bootstrap/dist/css/bootstrap.min.css"
import Title from "./Components/TitleComponent/TitleComponent";
import Form from "./Components/FormComponent/FormComponent";
function App() {
  return (
    <div className ="container mt-5">
        <Title/>
        <Form/>
    </div>
  );
}

export default App;
