import { Component } from "react";

class Form extends Component{
    onChangeInput(event){
        console.log(event.target.value)
    }
    onSelectCountry(event){
        console.log("Selecting country")
    }
    onSubmitClick(){
        console.log("Form đã được submit")
    }
    render (){
        return (
            <div className="bg-light rounded-3 mt-3" style={{padding:"5px"}}>
                <div className="form-group row m-2">
                        <label className="col-form-label col-3" >First Name</label>
                    <div className="col-9 text-right">
                        <input className=" form-control" placeholder="Your name..." onChange={this.onChangeInput}/>
                    </div>
                </div>
                <div className="form-group row m-2">
                        <label className="col-form-label col-3" >Last Name</label>
                    <div className="col-9 text-right">
                        <input className=" form-control" placeholder="Your last name..." onChange={this.onChangeInput}/>
                    </div>
                </div>
                <div className="form-group row m-2">
                        <label className="col-form-label col-3" >Country</label>
                    <div className="col-9 text-right">
                        <select className="form-control" onClick={this.onSelectCountry}>
                            <option>Australia</option>
                            <option>England</option>
                            <option>Singapore</option>
                        </select>
                    </div>
                </div>
                <div className="form-group row m-2">
                        <label className="col-form-label col-3" >Subject</label>
                    <div className="col-9 text-right">
                        <textarea className=" form-control" placeholder="Write something..." style={{height:"200px"}} onChange={this.onChangeInput}/>
                    </div>
                </div>
                <div className="m-3">
                    <button className="btn btn-success" type="submit" onClick={this.onSubmitClick}>Send Data</button>
                </div>
            </div>
        )
    }
}
export default Form;